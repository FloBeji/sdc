import {onManageActiveEffect, prepareActiveEffectCategories} from "../helpers/effects.mjs";

/**
 * Extend the basic ActorSheet with some very simple modifications
 * @extends {ActorSheet}
 */
export class SystemeDCritiqueActorSheet extends ActorSheet {

  /** @override */
  static get defaultOptions() {
    return mergeObject(super.defaultOptions, {
      classes: ["sdc", "sheet", "actor"],
      template: "systems/sdc/templates/actor/actor-sheet.html",
      width: 700,
      height: 600,
      tabs: [{ navSelector: ".sheet-tabs", contentSelector: ".sheet-body", initial: "attributes" }]
    });
  }

  /** @override */
  get template() {
    return `systems/sdc/templates/actor/actor-${this.actor.data.type}-sheet.html`;
  }

  /* -------------------------------------------- */

  /** @override */
  getData() {
    // Retrieve the data structure from the base sheet. You can inspect or log
    // the context variable to see the structure, but some key properties for
    // sheets are the actor object, the data object, whether or not it's
    // editable, the items array, and the effects array.
    const context = super.getData();

    // Use a safe clone of the actor data for further operations.
    const actorData = context.actor.data;

    // Add the actor's data to context.data for easier access, as well as flags.
    context.data = actorData.data;
    context.flags = actorData.flags;

    // Prepare character data and items.
    if (actorData.type == 'character') {
      this._prepareItems(context);
      this._prepareCharacterData(context);
    }

    // Add roll data for TinyMCE editors.
    context.rollData = context.actor.getRollData();

    // Prepare active effects
    context.effects = prepareActiveEffectCategories(this.actor.effects);

    return context;
  }

  /**
   * Organize and classify Items for Character sheets.
   *
   * @param {Object} actorData The actor to prepare.
   *
   * @return {undefined}
   */
  _prepareCharacterData(context) {
  }

  /**
   * Organize and classify Items for Character sheets.
   *
   * @param {Object} actorData The actor to prepare.
   *
   * @return {undefined}
   */
  _prepareItems(context) {
    // Initialize containers.
    const gear = [];
    const features = [];

    // Iterate through items, allocating to containers
    for (let i of context.items) {
      i.img = i.img || DEFAULT_TOKEN;
      // Append to gear.
      if (i.type === 'item') {
        gear.push(i);
      }
      // Append to features.
      else if (i.type === 'feature') {
        features.push(i);
      }
    }

    // Assign and return
    context.gear = gear;
    context.features = features;
   }

  /* -------------------------------------------- */

  /** @override */
  activateListeners(html) {
    super.activateListeners(html);

    // Render the item sheet for viewing/editing prior to the editable check.
    html.find('.item-edit').click(ev => {
      const li = $(ev.currentTarget).parents(".item");
      const item = this.actor.items.get(li.data("itemId"));
      item.sheet.render(true);
    });

    // -------------------------------------------------------------
    // Everything below here is only needed if the sheet is editable
    if (!this.isEditable) return;

    // Add Inventory Item
    html.find('.item-create').click(this._onItemCreate.bind(this));

    // Delete Inventory Item
    html.find('.item-delete').click(ev => {
      const li = $(ev.currentTarget).parents(".item");
      const item = this.actor.items.get(li.data("itemId"));
      item.delete();
      li.slideUp(200, () => this.render(false));
    });

    // Active Effect management
    html.find(".effect-control").click(ev => onManageActiveEffect(ev, this.actor));

    // Rollable abilities.
    html.find('.rollable').click(this._onRoll.bind(this));
    
    html.find('.savetextarea').click(event=> this.saveTextArea(html, event));

    // Drag events for macros.
    if (this.actor.owner) {
      let handler = ev => this._onDragStart(ev);
      html.find('li.item').each((i, li) => {
        if (li.classList.contains("inventory-header")) return;
        li.setAttribute("draggable", true);
        li.addEventListener("dragstart", handler, false);
      });
    }
  }

  saveTextArea (html, event){
    const element = event.currentTarget;
    const dataset = element.dataset;

    const textAreaList = html.find('textarea');
    if(!textAreaList || textAreaList.length < 1)
      return;
    const textArea = this.findElementById(textAreaList, dataset.target);
    if(!textArea)
      return;
      
    const inputList = html.find('input');
    if(!inputList || inputList.length < 1)
      return;
    const input = this.findElementById(inputList, dataset.target);
    if(!input)
      return;
    
      input.value = textArea.value;
  }

  findElementById(array, id){
    for(let i=0; i < array.length; i++)
      if(array[i].id === id)
        return array[i];
  }

  /**
   * Handle creating a new Owned Item for the actor using initial data defined in the HTML dataset
   * @param {Event} event   The originating click event
   * @private
   */
  async _onItemCreate(event) {
    event.preventDefault();
    const header = event.currentTarget;
    // Get the type of item to create.
    const type = header.dataset.type;
    // Grab any data associated with this control.
    const data = duplicate(header.dataset);
    // Initialize a default name.
    const name = `New ${type.capitalize()}`;
    // Prepare the item object.
    const itemData = {
      name: name,
      type: type,
      data: data
    };
    // Remove the type from the dataset since it's in the itemData.type prop.
    delete itemData.data["type"];

    // Finally, create the item!
    return await Item.create(itemData, {parent: this.actor});
  }

  _onRoll(event) {
    event.preventDefault();
    const element = event.currentTarget;
    const dataset = element.dataset;

    if(dataset.rollsdc) 
      this.buildDialog(this.actor.getRollData(), dataset.rollsdc);
  }

  getSdcValue(actorData, formula){
    console.log('p' + formula + 'p');

    if(formula.includes('+'))
      return eval(formula);

    if(Number.isInteger(parseInt(formula)))
      return formula;

    const attributes = this.getAllAtributes();
    let attribute = attributes.find(x=> x.key === formula);
    if(formula === 'f' || formula === 'F') 
      attribute = {key: 'FOR'};
    if(attribute) 
      return this.getAttribute(actorData, attribute.key).value;

    const skill= formula.length >= 3 ? this.getSkill(actorData, formula) : null;
    if(skill)
      return !skill.mod || parseInt(skill.mod) === 0 
        ? skill.rank 
        : eval(`${skill.rank} + ${skill.mod}`);

    return 1;
  }

  correctSdcRollValues(sdcRoll){
    const result = {...sdcRoll};
    if(result.diceNumber < 1) {
      result.boundary += -1 + result.diceNumber;
      result.diceNumber = 1;
    }
    
    if(result.boundary < 1) {
      result.boundary = 1;

      if(result.diceNumber > 1) result.diceNumber--;
    }

    if(result.boundary > 8){
      result.diceNumber += (result.boundary - 8);
      result.boundary = 8;
    }

    return result;
  }

  getAllAtributes(){
    return [
      {key:'FOR', name:'Force'},
      {key:'END', name:'Endurance'},
      {key:'DEX', name:'Dextérité'},
      {key:'RAP', name:'Rapidité'},
      {key:'PER', name:'Perception'},
      {key:'VOL', name:'Volonté'},
      {key:'INT', name:'Intelligence'},
      {key:'CHA', name:'Charisme'}];
  }

  buildAttributeOptions(key){
    const attributes = this.getAllAtributes();
    let result = attributes.map(x=> `<option value="${x.key}" ${key === x.key ? ' selected ' : ''}>${x.name}</option>`);
    result.push(`<option value="" ${attributes.find(x=> x.key ===key)  ? '' : ' selected '}>Autre</option>`);
    return result;
  }

  buildDialog(actorData, formula){
    const formulaArray = formula.split('/');
    const sdcRoll = {
      diceNumber : parseInt(this.getSdcValue(actorData, formulaArray[0].trim())),
      boundary : parseInt(this.getSdcValue(actorData, formulaArray[1].trim()))
    };
    const sdcRollCorrected = this.correctSdcRollValues(sdcRoll);
    const triggerButton1Click = "document.evaluate('//button[@data-button]', document, null, XPathResult.FIRST_ORDERED_NODE_TYPE, null).singleNodeValue.click()";
    new Dialog({
      title:'Rouler les dés !',
      content:`
        <form>
          <div class="form-group">
            <label>Attribut de Seuil :</label>
            <select id="attribut" onChange="${triggerButton1Click}">
              ${this.buildAttributeOptions(formulaArray[1])}
            </select>
          </div>
          <div class="form-group">
            <label>Formule :</label>
            <input disabled type="text" value="${formula}" />
          </div>
          <div class="form-group">
            <label>Valeurs :</label>
            <input disabled type="text" value="${sdcRoll.diceNumber}/${sdcRoll.boundary}" />
          </div>
          <div class="form-group">
            <label>Valeurs corrigées :</label>
            <input disabled type="text" value="${sdcRollCorrected.diceNumber}/${sdcRollCorrected.boundary}" />
          </div>
          <div class="form-group">
            <label>Nombre de dé :</label>
            <input id="diceNumber" type="number" value="${sdcRollCorrected.diceNumber}" />
          </div>
          <div class="form-group">
            <label>Seuil :</label> 
            <input id="boundary" type="number" value="${sdcRollCorrected.boundary}" />
          </div>
        </form>`,
      buttons:{
        no: {
          label: `Recharger`,
          callback: html => {
            const attribut = html.find('[id="attribut"]')[0].value;
            let newFormula = `${formulaArray[0]}/${attribut}`;
            if(!attribut) newFormula = formula;
            return this.buildDialog(actorData, newFormula);
          }
        },
        yes: {
          icon: "<i class='fas fa-dice'></i>",
          label: `Rouler les dés`,
          callback: html => {
            const diceNumberFinal = html.find('[id="diceNumber"]')[0].value;
            const boundaryFinal = html.find('[id="boundary"]')[0].value;
            this.roll(actorData, `${diceNumberFinal}/${boundaryFinal} ${formula}`,diceNumberFinal ,boundaryFinal );
          }
        }
      },
      default:'yes'
    }).render(true);
  }

  roll(actorData, label, rank, boundary){
    const formula = `(${rank})d10cs<=${boundary}x1cs<=${boundary}`;
    const roll = new Roll(formula, actorData);
    roll.toMessage({
      speaker: ChatMessage.getSpeaker({ actor: this.actor }),
      flavor: label,
      rollMode: game.settings.get('core', 'rollMode'),
    });
    return roll;
  }

  getSkill(obj, name){    
    return Object.getOwnPropertyNames(obj.skills)
      .map(x=> obj.skills[x])
      .find(x=> x.name === name);
  }

  getAttribute(obj, key){
    const realKey = key === "f" || key === "F" ? "FOR" : key;
    return Object.getOwnPropertyNames(obj.attributes)
      .map(x=> obj.attributes[x])
      .find(x=> x.key === realKey);
  }
}
